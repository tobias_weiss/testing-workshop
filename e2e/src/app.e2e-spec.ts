import {LoginComponent} from '../../src/app/login/login.component';
import {browser, by, element} from "protractor";


describe('LoginComponent', () => {
  it('should have a title', function() {
    browser.get('http://localhost:4200');

    expect(browser.getTitle()).toEqual('TestingWorkshop');
  });
  it('should provide input fields for the login', function() {
    browser.get('http://localhost:4200');
    element(by.name('mail')).sendKeys('holger.holgerson@bytabo.de');
    element(by.id('loginButton')).click();

    expect(browser.getCurrentUrl()).toContain('/main');
  });


});
