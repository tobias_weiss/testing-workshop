import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {User} from "../models/user.model";
import {Router} from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  get currentUser(): User {
    return this._currentUser;
  }

  set currentUser(value: User) {
    this._currentUser = value;
  }

  private _currentUser: User;

  private httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient, private router: Router) {
  }

  /* isAuthenticated(): boolean {
     return !!localStorage.getItem('token');
   }*/

  login(mail: string, password: string) {
    return this.http.post<any>(`http://localhost:8000/login`, {
      user: {
        email: mail,
        password: password
      }
    }, this.httpOptions).subscribe(
      (user: User) => {
        if (user) {
          localStorage.setItem('testingUser', JSON.stringify(user));
          this._currentUser = user;
          this.navigate('main');
        }
      },
      error => {
        alert(error.error.message);
      })
  }

  navigate(route: String){
    this.router.navigate([route]);
  }
}
