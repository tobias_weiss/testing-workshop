import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./main/main.component";
import {AppComponent} from "./app.component";
import {LoginComponent} from "./login/login.component";

export const routes: Routes = [
  {path: "", component: LoginComponent},
  {path:'main', component: MainComponent}
];

export const routing = RouterModule.forRoot(routes,    { anchorScrolling: 'enabled', scrollPositionRestoration: 'enabled'});
