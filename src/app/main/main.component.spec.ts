import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MainComponent} from './main.component';
import {FormsModule} from "@angular/forms";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {RouterTestingModule} from "@angular/router/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";
import {LoginComponent} from "../login/login.component";
import {HttpClientTestingModule} from "@angular/common/http/testing";



describe('MainComponent', () => {
  let component: MainComponent;
  let service: AuthService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainComponent],
      imports: [FormsModule, MatFormFieldModule, RouterTestingModule.withRoutes([]
      ), MatInputModule, BrowserAnimationsModule, HttpClientTestingModule],
      providers: [AuthService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    service = TestBed.get(AuthService);
    component = new MainComponent(router, service);
  });

  it("should be created", () => {
    expect(component).toBeTruthy();
  });
});
