import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {User} from "../models/user.model";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  currentUser: User;

  constructor(private router: Router, private auth: AuthService) {
  }

  ngOnInit() {
    this.currentUser = this.auth.currentUser;
  }

  logout() {
    this.router.navigate([''])
  }

}
