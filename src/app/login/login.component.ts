import {Component, OnInit} from '@angular/core';
import {User} from "../models/user.model";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  mail: string;
  password: string;

  constructor(private auth: AuthService, private router: Router) {
  }


  login(mail: string, password: string) {
    this.auth.login(mail, password);
  }


}
