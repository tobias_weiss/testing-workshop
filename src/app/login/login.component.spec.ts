import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {FormsModule} from "@angular/forms";
import {MatFormFieldModule, MatInputModule} from "@angular/material";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {MainComponent} from "../main/main.component";
import {routes} from "../app.routing";
import {Location} from "@angular/common";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let service: AuthService;
  let router: Router;
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, MainComponent],
      imports: [FormsModule, MatFormFieldModule, HttpClientTestingModule, RouterTestingModule.withRoutes(routes), MatInputModule, BrowserAnimationsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    location = TestBed.get(Location);
    router = TestBed.get(Router);
    service = TestBed.get(AuthService);
    component = new LoginComponent(service, router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //Test using spy to use function from auth.service
  it('login() should call the AuthServices login() if triggered', () => {
    const spy = spyOn(service, 'login').and.callThrough();
    component.mail = 'holger.holgerson@bytabo.de';
    component.password = 'test';
    component.login(component.mail, component.password);
    expect(service.login).toHaveBeenCalled();
    expect(service.login).toHaveBeenCalledWith('holger.holgerson@bytabo.de', 'test');
  });

  //First end-2-end test
  

});
